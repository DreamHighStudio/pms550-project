import mysql from 'mysql';

const con = mysql.createConnection({
    host: 'database-2.cwcyigoaudde.us-east-1.rds.amazonaws.com',
    user: 'admin',
    password: 'December12',
    port: '3306'
});
const DEFAULT_DB_SQL = "USE `MIS550`;";

async function create_prof(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "INSERT INTO `tblTeachers` (`TeacherFName`,`TeacherMidName`,`TeacherLastName`,`TeacherSuffix`,`TeacherMinSemClass`,`TeacherMaxSemClass`,`TeacherMaxYearClass`) VALUES ('" + req.body.firstName + "','" + req.body.middleName + "','" + req.body.lastName + "','" + req.body.suffix + "','" + req.body.minClassSem + "','" + req.body.maxClassSem + "','" + req.body.maxClassYear + "');";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.json(200);
                }
            });
        }
    });
}

async function get_prof(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "SELECT * FROM tblTeachers;";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.send(result);
                }
            });
        }
    });
}

async function delete_prof(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "DELETE FROM `tblTeachers` WHERE `TeacherID`=" + req.body.id + ";";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.json(200);
                }
            });
        }
    });
}

async function create_course(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "INSERT INTO `tblCourse` (`CourseNumber`,`CourseTitle`,`CourseCredits`,`CourseMinPerWeek`) VALUES ('" + req.body.courseNumber + "','" + req.body.courseTitle + "','" + req.body.courseCredits + "','" + req.body.courseMinPerWeek + "');";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    console.log(result);
                    var sql = "INSERT INTO `tblTeachCourse` (`FK_CourseID`,`FK_TeacherID`) VALUES ('" + result.insertId + "','" + req.body.profID + "');";
                    await con.query(sql, async function (err, result) {
                        if (err) {
                            res.status(400).send(err);
                        } else {
                            res.json(200);
                        }
                    });
                }
            });
        }
    });
}

//TODO: fix join to show all courses
async function get_course(reg, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "select * from tblTeachCourse P  join tblCourse C on P.FK_CourseID = C.CourseID  join tblTeachers T on P.FK_TeacherID = T.TeacherID;";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.send(result);
                }
            });
        }
    });
}

//TODO: Need to fix to delete references from Section table
async function delete_course(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "DELETE FROM `tblTeachCourse` WHERE `TeachCourseID`=" + req.body.teachCourseIID + ";";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to delete new record");
                } else {
                    var sql = "DELETE FROM `tblCourse` WHERE `CourseID`=" + req.body.courseID + ";";
                    await con.query(sql, async function (err, result) {
                        if (err) {
                            res.status(400).send("Failed to delete new record");
                        } else {
                            res.json(200);
                        }
                    });
                }
            });
        }
    });
}

async function create_semester(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "INSERT INTO `tblSemester` (`SemesterName`,`SemesterYear`,`SemesterStartDate`,`SemesterEndDate`) VALUES ('" + req.body.semesterName + "','" + req.body.year + "','" + req.body.semesterStartDate + "','" + req.body.semesterEndDate + "');";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.json(200);
                }
            });
        }
    });
}

async function get_semester(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "SELECT * FROM tblSemester;";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.send(result);
                }
            });
        }
    });
}

async function delete_semester(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "DELETE FROM `tblSection` WHERE `FK_SemesterID`=" + req.body.semesterID + ";";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to delete new record");
                } else {
                    var sql = "DELETE FROM `tblSemester` WHERE `SemesterID`=" + req.body.semesterID + ";";
                    await con.query(sql, async function (err, result) {
                        if (err) {
                            res.status(400).send("Failed to delete new record");
                        } else {
                            res.json(200);
                        }
                    });
                }
            });
        }
    });
}

async function create_building(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "INSERT INTO `TblBuilding` (`BldgName`) VALUES ('" + req.body.buildingName + "');";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.json(200);
                }
            });
        }
    });
}

async function get_building(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "SELECT * FROM TblBuilding;";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.send(result);
                }
            });
        }
    });
}

async function delete_building(req, res, next) {

}

async function create_room(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "INSERT INTO `tblRoom` (`FK_BldgID`, `RoomName`) VALUES ('" + req.body.buildingID + "','" + req.body.roomName + "');";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.json(200);
                }
            });
        }
    });
}

async function get_room(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "select * from tblRoom R  join TblBuilding B on R.FK_BldgID = B.BldgID;";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.send(result);
                }
            });
        }
    });
}

async function delete_room(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "DELETE FROM `tblRoom` WHERE `RoomID`=" + req.body.roomID + ";";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.json(200);
                }
            });
        }
    });
}

async function create_section(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "INSERT INTO `tblSection` (`FK_CourseID`, `SectionNo`, `FK_SemesterID`, `SectionMeetPerWeek`, `SectionMeetMinutes`) VALUES ('" + req.body.selectedCourseID + "','" + req.body.sectionNo + "','" + req.body.selectedSemID + "','" + req.body.sectionMeetingWeek + "','" + req.body.sectionMeetMin + "');";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.json(200);
                }
            });
        }
    });
}

async function get_section(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "SELECT * FROM tblSection;";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.send(result);
                }
            });
        }
    });
}

async function delete_section(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "DELETE FROM `tblSection` WHERE `SectionID`=" + req.body.sectionID + ";";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    res.json(200);
                }
            });
        }
    });
}

async function get_course_prof(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "select * from tblTeachCourse Z join tblCourse C on Z.FK_CourseID = C.CourseID join tblTeachers T on Z.FK_TeacherID = T.TeacherID;";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.send(result);
                }
            });
        }
    });
}

async function get_course_schedule(reeq, res) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "select * from tblCourseSchedule A join tblTimeSlots T on A.FK_TSID = T.TSID join tblTeachers B on A.FK_TeacherID = B.TeacherID join tblRoom R on A.FK_RoomID = R.RoomID join tblSection S on A.FK_SectionID = S.SectionID ;";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    res.send(result);
                }
            });
        }
    });
}

async function create_schedule(req, res, next) {
    await con.query(DEFAULT_DB_SQL, async function (err, result) {
        if (err) {
            res.status(400).send("Failed to create new record");
        } else {
            var sql = "INSERT INTO `tblTimeSlots` (`TSDay`, `TSStartTime`, `TSDuration`) VALUES ('" + req.body.date + "','" + req.body.startTime + "','" + req.body.duration + "');";
            await con.query(sql, async function (err, result) {
                if (err) {
                    res.status(400).send("Failed to create new record");
                } else {
                    var sql = "INSERT INTO `tblCourseSchedule` (`FK_SectionID`, `FK_TSID`, `FK_TeacherID`, `FK_RoomID`, `TentativeYN`) VALUES ('" + req.body.sectionID + "','" + result.insertId+ "','" + req.body.teacherID + "','" + req.body.roomID + "','" + req.body.tentative+ "');";
                    await con.query(sql, async function (err, result) {
                        if (err) {
                            res.status(400).send("Failed to create new record");
                        } else {
                            res.json(200);
                        }
                    });
                }
            });
        }
    });
}

//
// async function signUp(req, res, next) {
//     await Users.findOne({email: req.body.email}, async (err, result) => {
//         if (result != null) {
//             // Error code 1001 duplicate account found associated with this account.
//             res.json(1001)
//         } else {
//             let hash = await Users.hashPassword(req.body.password);
//             let user = new Users();
//             user.name = req.body.name;
//             user.email = req.body.email;
//             user.password = hash;
//             user.save().then(user => {
//                 res.json(202);
//             }).catch(err => {
//                 res.status(400).send("Failed to create new record");
//             });
//         }
//     });
// }
//
// function dashbored(req, res, next) {
//     // console.log("dashbored");
//     res.json("dashbored works");
// }


module.exports = {
    create_prof,
    get_prof,
    delete_prof,
    create_course,
    get_course,
    delete_course,
    create_semester,
    get_semester,
    delete_semester,
    create_building,
    get_building,
    delete_building,
    create_room,
    get_room,
    delete_room,
    create_section,
    get_section,
    delete_section,
    get_course_prof,
    get_course_schedule,
    create_schedule,

};
