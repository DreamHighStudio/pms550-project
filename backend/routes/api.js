// user API's
import express from 'express';

const router = express.Router();

// Controllers
import request_controller from '../controllers/requests';

router.post('/create_prof', request_controller.create_prof);
router.get('/get_prof', request_controller.get_prof);
router.post('/delete_prof', request_controller.delete_prof);
router.post('/create_course', request_controller.create_course);
router.get('/get_course', request_controller.get_course);
router.post('/delete_course', request_controller.delete_course);
router.post('/create_semester', request_controller.create_semester);
router.get('/get_semester', request_controller.get_semester);
router.post('/delete_semester', request_controller.delete_semester);
router.post('/create_building', request_controller.create_building);
router.get('/get_building', request_controller.get_building);
router.post('/delete_building', request_controller.delete_building);
router.post('/create_room', request_controller.create_room);
router.get('/get_room', request_controller.get_room);
router.post('/delete_room', request_controller.delete_room);
router.post('/create_section', request_controller.create_section);
router.get('/get_section', request_controller.get_section);
router.post('/delete_section', request_controller.delete_section);
router.get('/get_course_prof', request_controller.get_course_prof);
router.get('/get_course_schedule', request_controller.get_course_schedule);
router.post('/create_schedule', request_controller.create_schedule);



module.exports = router;
