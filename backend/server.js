import "@babel/polyfill";
import express from 'express';
import cors from 'cors'
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser'


// *** Routes ***
import api from './routes/api'

const app = express();

// *** Middleware ***
app.use(cookieParser());

// *** Parsing the JSON requests ***
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));



// connection.connect(function(err) {
//   if (err) throw err;
//   console.log("Connected!");
//   connection.query("SELECT * FROM MIS550.TblBuilding", function (err, result,fields) {
//     if (err) throw err;
//     Object.keys(result).forEach(function(key) {
//       var row = result[key];
//       console.log(row)
//     });
//   });
// });
// Test Database connection
// connection.connect(function (err) {
//     if (err) {
//         console.error('Database connection failed: ' + err.stack);
//         return;
//     }
//
//     console.log('Connected to database.');
// });
app.use(cors());


//Users API Routes
app.use('/api', api);


app.listen(4000, () => console.log('Server running on port 4000'));

