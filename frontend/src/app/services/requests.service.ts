import {Injectable} from '@angular/core';
import {config} from '../../config';
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private http: HttpClient, private router: Router) {
  }

  createProfessor(prof) {
    return this.http.post<any>(`${config.api_uri}/api/create_prof`, prof);
  }

  getProfessor() {
    return this.http.get<any>(`${config.api_uri}/api/get_prof`);
  }

  deleteProfessor(prof) {
    return this.http.post<any>(`${config.api_uri}/api/delete_prof`, prof);
  }

  createCourse(course) {
    return this.http.post<any>(`${config.api_uri}/api/create_course`, course);
  }

  getCourse() {
    return this.http.get<any>(`${config.api_uri}/api/get_course`);
  }

  deleteCourse(keys) {
    return this.http.post<any>(`${config.api_uri}/api/delete_course`, keys);
  }

  createSemester(semester) {
    return this.http.post<any>(`${config.api_uri}/api/create_semester`, semester);
  }

  getSemester() {
    return this.http.get<any>(`${config.api_uri}/api/get_semester`);
  }

  deleteSemester(key) {
    return this.http.post<any>(`${config.api_uri}/api/delete_semester`, key);
  }

  createBuilding(building) {
    return this.http.post<any>(`${config.api_uri}/api/create_building`, building);
  }

  getBuilding() {
    return this.http.get<any>(`${config.api_uri}/api/get_building`);
  }

  // deleteBuilding(building) {
  //   return this.http.post<any>(`${config.api_uri}/api/delete_building`, building);
  // }
  createRoom(room) {
    return this.http.post<any>(`${config.api_uri}/api/create_room`, room);
  }

  getRoom() {
    return this.http.get<any>(`${config.api_uri}/api/get_room`);
  }

  deleteRoom(roomID) {
    return this.http.post<any>(`${config.api_uri}/api/delete_room`, roomID);
  }

  createSection(section) {
    return this.http.post<any>(`${config.api_uri}/api/create_section`, section);
  }

  getSection() {
    return this.http.get<any>(`${config.api_uri}/api/get_section`);
  }

  deleteSection(roomID) {
    return this.http.post<any>(`${config.api_uri}/api/delete_section`, roomID);
  }

  getCourseProfessor(course) {
    return this.http.get<any>(`${config.api_uri}/api/get_course_prof`);
  }

  creatSchedule(schedule) {
    return this.http.post<any>(`${config.api_uri}/api/create_schedule`, schedule);
  }

  getSchedule(){
    return this.http.get<any>(`${config.api_uri}/api/get_course_schedule`);
  }

  deleteSchedule(schduleID){

  }
}
