import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {of, Observable} from "rxjs";
import {catchError, mapTo, tap} from "rxjs/operators";
import {config} from '../../config';
import {Tokens} from '../models/tokens'
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private loggedUser: string;

  constructor(private http: HttpClient, private router: Router) {
  }

  login(email, password) {
    const user = {
      email: email,
      password: password
    };
    return this.http.post<any>(`${config.api_uri}/users/login`, user)
      .pipe(
        tap(tokens => this.doLoginUser(user.email, tokens)),
        catchError(error => {
          alert(error.error);
          return of(false);
        }));
  }

  logout() {
    this.doLogoutUser();
  }

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  private doLoginUser(email: string, tokens: Tokens) {
    this.loggedUser = email;
    this.storeTokens(tokens);
  }

  private doLogoutUser() {
    this.loggedUser = null;
    this.removeTokens();
    this.router.navigate(['/login']);
  }

  private storeTokens(tokens: Tokens) {
    localStorage.setItem(this.JWT_TOKEN, tokens.jwt);
  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
  }

  registerUser(name, email, passwordConfirmation) {
    const user = {
      name: name,
      email: email,
      password: passwordConfirmation,
    };
    return this.http.post(`${config.api_uri}/users/signup`, user);
  }
}
