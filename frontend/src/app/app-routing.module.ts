import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CourseLookupComponent} from "./components/course-lookup/course-lookup.component";
import {CoursePlannerComponent} from "./components/course-planner/course-planner.component";
import {LoginComponent} from "./components/login/login.component";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {SemesterComponent} from "./components/semester/semester.component";
import {ProfessorComponent} from "./components/professor/professor.component";
import {AddCourseComponent} from "./components/add-course/add-course.component";
import {CalendarComponent} from "./components/calendar/calendar.component";
import {FacilitiesComponent} from "./components/facilities/facilities.component";
import {SectionComponent} from "./components/section/section.component";


const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'section',
    component: SectionComponent
  },
  {
    path: 'course_planner',
    component: CoursePlannerComponent
  },
  {
    path: 'add_course',
    component: AddCourseComponent
  },
  {
    path: 'professor',
    component: ProfessorComponent
  },
  {
    path: 'course_lookup',
    component: CourseLookupComponent
  },
  {
    path: 'facilities',
    component: FacilitiesComponent
  },
  {
    path: 'calendar',
    component: CalendarComponent
  },
  {
    path: 'semester',
    component: SemesterComponent
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
