import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";

import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';


import {UserService} from './services/user.service'
import {ValidateService} from "./services/validate.service";
import {CourseLookupComponent} from './components/course-lookup/course-lookup.component';
import {CoursePlannerComponent} from "./components/course-planner/course-planner.component";
import {SemesterComponent} from './components/semester/semester.component';
import {LoginComponent} from './components/login/login.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {ProfessorComponent} from './components/professor/professor.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AddCourseComponent} from './components/add-course/add-course.component';
import {CalendarComponent} from './components/calendar/calendar.component';
import {FacilitiesComponent} from './components/facilities/facilities.component';
import { SectionComponent } from './components/section/section.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CourseLookupComponent,
    CoursePlannerComponent,
    SemesterComponent,
    LoginComponent,
    DashboardComponent,
    ProfessorComponent,
    AddCourseComponent,
    CalendarComponent,
    FacilitiesComponent,
    SectionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,

  ],
  providers: [
    UserService,
    ValidateService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
