import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RequestsService} from "../../services/requests.service";

@Component({
  selector: 'app-course-planner',
  templateUrl: './course-planner.component.html',
  styleUrls: ['./course-planner.component.scss']
})
export class CoursePlannerComponent implements OnInit {
  stage = 1;
  showConfirmBtn = false;
  semester: any;
  course: any;
  section: any;
  building: any;
  room: any;
  professor: any;
  day: any;
  allSemester: any;
  selectedSemester: any;
  allCourses: any;
  isCourseDisabled = true;
  selectedCourse: any;
  isSectionDisabled = true;
  isProfDisabled = true;
  allSection: any;
  selectedSection: any;

  allRooms: any;
  allBuilding: any;
  isRoomDisabled = true;
  selectedRoom: any;

  isProfessorDisabled = true;
  allProf: any;

  selectedProfessor: any;
  startTime = {hour: 8, minute: 30};
  endTime = {hour: 15, minute: 30};
  date: any;

  message: string;
  showAlert: boolean;

  tentative = false;

  selectedProfName: string;

  constructor(private router: Router, private requestsService: RequestsService) {
  }

  ngOnInit() {
    this.loadSemester();
    this.loadBuilding();
  }

  loadSemester() {
    this.requestsService.getSemester().subscribe((result) => {
      this.allSemester = result;
    });
  }

  loadBuilding() {
    this.requestsService.getBuilding().subscribe((result) => {
      this.allBuilding = result;
    });
  }

  async confirmSelection() {
    //TODO: min coversion properly this will just not work it will be 0
    let timeDiffHour = this.endTime.hour - this.startTime.hour;
    let timeDiffMin = this.endTime.minute - this.startTime.minute;
    let minConvt = (timeDiffHour * 60) + timeDiffMin;
    let newDate = this.date.month + "-" + this.date.day + "-" + this.date.year;
    let newTime = this.startTime.hour + ":" + this.startTime.minute;

    let isVaildSelection = true;
    this.requestsService.getSchedule().subscribe(async (result) => {
      console.log(result);
      if (result.length == 0) {
        let schedule = {
          sectionID: this.selectedSection,
          teacherID: this.selectedProfessor,
          roomID: this.selectedRoom,
          date: newDate,
          startTime: newTime,
          duration: minConvt,
          tentative: this.tentative
        };
       await this.requestsService.creatSchedule(schedule).subscribe(async (res) => {
          if (res == "200") {
            this.message = "Schedule is added successfully";
            this.showAlert = true;
            // this.modalService.dismissAll("success");
            // this.loadCourse();
            this.router.navigate(["course_planner"]);
          } else {
            // GGWP
          }
        });
      } else {
         for (let val of result) {
          let tempLowerTimeHour = val.TSStartTime.split(":")[0];
          let tempLowerTimeMin = val.TSStartTime.split(":")[1];

          let tempHour = (Math.floor(val.TSDuration / 60)) + ((tempLowerTimeHour * 60) / 60);
          let tempMin = val.TSDuration % 60;
          let timeObject = {hour: tempHour, minute: tempMin};

            if ((this.startTime.hour >= tempLowerTimeHour && this.startTime.minute >= tempLowerTimeMin) && (this.endTime.hour <= timeObject.hour)) {
              if(val.TeacherLastName == this.selectedProfName){
               isVaildSelection = false;
              }

            }
          }
         console.log("here");
         if(!isVaildSelection){
           this.message = "This time slot is not available. Try again."
           this.showAlert =true;
         }else {
           let schedule = {
             sectionID: this.selectedSection,
             teacherID: this.selectedProfessor,
             roomID: this.selectedRoom,
             date: newDate,
             startTime: newTime,
             duration: minConvt,
             tentative: this.tentative
           };
           await this.requestsService.creatSchedule(schedule).subscribe(async (res) => {
             if (res == "200") {
               this.showAlert = true;
               this.message = "You are all booked. Set a reminder and I will see you at: "+ this.startTime.hour +":"+ this.startTime.minute;
               // this.modalService.dismissAll("success");
               // this.loadCourse();
               this.router.navigate(["course_planner"]);
             } else {
               // GGWP
             }
           });
         }

        }
    });
    //console.log(minConvt);
    // let temp = minConvt % 60
    // console.log(temp);

  // else{
  //     let schedule = {
  //       sectionID: this.selectedSection,
  //       teacherID: this.selectedProfessor,
  //       roomID: this.selectedRoom,
  //       date: newDate,
  //       startTime: newTime,
  //       duration: minConvt,
  //       tentative: this.tentative
  //     };
  //     this.requestsService.creatSchedule(schedule).subscribe((res) => {
  //       if (res == "200") {
  //         this.message = "Schedule is added successfully";
  //         this.showAlert = true;
  //         // this.modalService.dismissAll("success");
  //         // this.loadCourse();
  //         this.router.navigate(["course_planner"]);
  //       } else {
  //         // GGWP
  //       }
  //     });
  //   }
  }

  selectSemester(semID) {
    this.selectedSemester = semID;
    this.requestsService.getCourse().subscribe((result) => {
      this.isCourseDisabled = false;
      this.allCourses = result;
    });
  }

  selectCourse(courseID) {
    this.selectedCourse = courseID;
    this.requestsService.getSection().subscribe((result) => {
      let temp = [];
      let temp2 = [];
      for (let sec of result) {
        if (sec.FK_CourseID == courseID && sec.FK_SemesterID == this.selectedSemester) {
          temp.push(sec);
        }
      }
      for (let cors of this.allCourses) {
        if (cors.FK_CourseID == courseID) {
          temp2.push(cors);
        }
      }
      this.allSection = temp;
      this.allProf = temp2;
      this.isSectionDisabled = false;
      this.isProfDisabled = false;
    });
  }

  selectSection(sectionID) {
    this.selectedSection = sectionID;
  }

  selectBuilding(buildingID) {
    this.requestsService.getRoom().subscribe((result) => {
      let temp = [];
      for (let room of result) {
        if (room.FK_BldgID == buildingID) {
          temp.push(room);
        }
      }
      this.allRooms = temp;
      this.isRoomDisabled = false;
    });
  }

  selectRoom(roomID) {
    this.selectedRoom = roomID;
  }

  selectProfessor(prof, name) {
    this.selectedProfessor = prof;
    this.selectedProfName = name;
  }

  selectTentative(value) {
    this.tentative = value;
  }
}
