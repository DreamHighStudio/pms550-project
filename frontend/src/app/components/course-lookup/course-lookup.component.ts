import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RequestsService} from "../../services/requests.service";

@Component({
  selector: 'app-course-lookup',
  templateUrl: './course-lookup.component.html',
  styleUrls: ['./course-lookup.component.scss']
})
export class CourseLookupComponent implements OnInit {
  selectedValue: any;
  name: string;
  allCourses: any;
  msg = false;

  constructor(private activateRoute: ActivatedRoute, private router: Router, private requestsService: RequestsService) {
    this.activateRoute.params.subscribe(params => {
      this.selectedValue = params.name;
    });
  }

  ngOnInit() {
  }
  loadCourse(){
    this.requestsService.getCourse().subscribe((result) => {
      if (this.name !=  undefined){
        let temp = [];
        for (let item of result){
          if(item.TeacherFName == this.name){
            temp.push(item);
          }
        }
        console.log(temp);
        if(temp.length == 0){
          this.allCourses = [];
          this.msg = true;
        }else {
          this.msg = false;
          this.allCourses = temp;
        }

      }
      console.log(this.allCourses);
    });
  }
  confirmName() {
    this.loadCourse();
    this.router.navigate(['course_lookup']);
  }

}
