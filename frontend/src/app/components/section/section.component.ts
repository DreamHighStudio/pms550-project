import {Component, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {RequestsService} from "../../services/requests.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {
  closeResult: string;
  allSemester: any;
  allCourses: any;
  selectedSemID: any;
  selectedCourseID: any;
  sectionNo: any;
  sectionMeetingWeek: any;
  sectionMeetMin: any;
  message: string;
  showAlert: any;

  allSection: any;

  constructor(private requestsService: RequestsService, private modalService: NgbModal, private router: Router) {
  }

  ngOnInit() {
    this.loadSemester();
    this.loadCourse();
    this.loadSection();
  }

  loadSection() {
    this.requestsService.getSection().subscribe((result) => {
      this.allSection = result;
    });
  }

  loadSemester() {
    this.requestsService.getSemester().subscribe((result) => {
      this.allSemester = result;
    });
  }

  loadCourse() {
    this.requestsService.getCourse().subscribe((result) => {
      this.allCourses = result;
    });
  }


  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  selectSemester(semID) {
    this.selectedSemID = semID;
  }

  selectCourse(courseID) {
    this.selectedCourseID = courseID;
  }

  createSection() {
    let section = {
      selectedSemID: this.selectedSemID,
      selectedCourseID: this.selectedCourseID,
      sectionNo: this.sectionNo,
      sectionMeetingWeek: this.sectionMeetingWeek,
      sectionMeetMin: this.sectionMeetMin
    };
    this.requestsService.createSection(section).subscribe((res) => {
      if (res == "200") {
        this.message = "Curse is added successfully";
        this.showAlert = true;
        this.modalService.dismissAll("success");
        this.loadSection();
        this.router.navigate(["section"]);
      } else {
        // GGWP
      }
    });
  }

  deleteSection(sectionID){
    let keys = {
      sectionID: sectionID,
    };
    this.requestsService.deleteSection(keys).subscribe( (res) =>{
      if (res == "200") {
        this.loadSection();
        this.router.navigate(["section"]);
      }else {
        // GGWP
      }
    })
  }
}
