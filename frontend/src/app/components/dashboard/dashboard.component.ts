import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  schedulePage() {
    this.router.navigate(['course_planner'])
  }

  professorPage() {
    this.router.navigate(['professor'])
  }

  coursePage() {
    this.router.navigate(['course_lookup'])
  }
  addCoursePage() {
    this.router.navigate(['add_course'])
  }
  addSemester() {
    this.router.navigate(['semester'])
  }
  addFacilities() {
    this.router.navigate(['facilities'])
  }
  addSection(){
    this.router.navigate(['section'])
  }



}
