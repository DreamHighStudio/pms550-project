import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  dropdownValue: any;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  confirmSelection() {
    if (this.dropdownValue == 'Selector') {
      this.router.navigate(['dashboard'])

    } else if (this.dropdownValue == 'Professor') {
      this.router.navigate(['dashboard'])
    }
  }

}
