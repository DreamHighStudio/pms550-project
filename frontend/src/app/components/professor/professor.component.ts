import {Component, OnInit} from '@angular/core';
import {RequestsService} from "../../services/requests.service";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router";

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.scss']
})
export class ProfessorComponent implements OnInit {
  firstName: string;
  middleName: string;
  lastName: string;
  suffix: string = "";
  minClassSem: number;
  maxClassSem: number;
  maxClassYear: number;
  closeResult: string;
  message: string;
  showAlert = false;
  current_profs: any;

  constructor(private requestsService: RequestsService, private modalService: NgbModal, private router: Router) {
  }

  ngOnInit() {
    this.loadProf();
  }

  loadProf() {
    this.requestsService.getProfessor().subscribe((result) => {
      this.current_profs = result;
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  createProfessor() {
    let prof = {
      firstName: this.firstName,
      middleName: this.middleName,
      lastName: this.lastName,
      suffix: this.suffix,
      minClassSem: this.minClassSem,
      maxClassSem: this.maxClassSem,
      maxClassYear: this.maxClassYear
    };
    //TODO: Do we care able no inputs?
    this.requestsService.createProfessor(prof).subscribe((res) => {
      if (res == "200") {
        this.message = "Professor is added successfully";
        this.showAlert = true;
        this.modalService.dismissAll("success");
        this.loadProf();
        this.router.navigate(["professor"]);
      } else {
        //oo snap something broke :|
      }
    });
  }

  deleteProf(prof_id) {
    let prof = {
      id: prof_id
    };
    this.requestsService.deleteProfessor(prof).subscribe((res) => {
      if (res == "200") {
        this.loadProf();
        this.router.navigate(["professor"]);
      }else {
        // GGWP
      }
    })
  }
}
