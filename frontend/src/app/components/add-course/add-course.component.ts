import {Component, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {RequestsService} from "../../services/requests.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.scss']
})
export class AddCourseComponent implements OnInit {
  closeResult: string;
  selectedProfessor: any;
  current_profs: any;
  courseNumber: any;
  courseTitle: any;
  courseCredits: any;
  courseMinPerWeek: any;
  message: string;
  showAlert = false;
  allCourses: any;

  constructor(private requestsService: RequestsService, private modalService: NgbModal, private router: Router) {
  }

  ngOnInit() {
    this.loadProf();
    this.loadCourse();
  }

  loadProf() {
    this.requestsService.getProfessor().subscribe((result) => {
      this.current_profs = result;
    });
  }

  loadCourse(){
    this.requestsService.getCourse().subscribe((result) => {
      this.allCourses = result;
      console.log(this.allCourses);
    });
  }



  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  selectProfess(profID) {
    this.selectedProfessor = profID;
  }

  creatCourse() {
    let course = {
      profID: this.selectedProfessor,
      courseNumber: this.courseNumber,
      courseTitle: this.courseTitle,
      courseCredits: this.courseCredits,
      courseMinPerWeek: this.courseMinPerWeek,
    };
    this.requestsService.createCourse(course).subscribe((res) => {
      if (res == "200") {
        this.message = "Curse is added successfully";
        this.showAlert = true;
        this.modalService.dismissAll("success");
        this.loadCourse();
        this.router.navigate(["add_course"]);
      }else {
        // GGWP
      }
    });
  }
  deleteCourse(teachCourseIID, courseID){
      let keys = {
        teachCourseIID: teachCourseIID,
        courseID: courseID
      }
      this.requestsService.deleteCourse(keys).subscribe( (res) =>{
        if (res == "200") {
          this.loadCourse();
          this.router.navigate(["add_course"]);
        }else {
          // GGWP
        }
      })
  }
}
