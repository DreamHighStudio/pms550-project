import {Component, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";
import {RequestsService} from "../../services/requests.service";

@Component({
  selector: 'app-semester',
  templateUrl: './semester.component.html',
  styleUrls: ['./semester.component.scss']
})
export class SemesterComponent implements OnInit {
  closeResult: string;
  semesterName: string;
  year: number;
  semesterStartDate: any;
  semesterEndDate: any;
  message: string;
  showAlert = false;
  allSemester: any;

  constructor(private modalService: NgbModal, private router: Router, private requestsService: RequestsService) {
  }

  ngOnInit() {
    this.loadSemester();
  }

  loadSemester() {
    this.requestsService.getSemester().subscribe((result) => {
      this.allSemester = result;
      console.log(this.allSemester);
    });
  }


  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  createSemester() {
    let semester = {
      semesterName: this.semesterName,
      year: this.year,
      semesterStartDate: this.semesterStartDate,
      semesterEndDate: this.semesterEndDate
    };
    this.requestsService.createSemester(semester).subscribe((res) => {
      if (res == "200") {
        this.message = "Semester is added successfully";
        this.showAlert = true;
        this.modalService.dismissAll("success");
        this.loadSemester();
        this.router.navigate(["semester"]);
      } else {
        // GGWP
      }
    });
  }
  deleteSemester(semesterID){
    let keys = {
      semesterID: semesterID,
    };
    this.requestsService.deleteSemester(keys).subscribe( (res) =>{
      if (res == "200") {
        this.loadSemester();
        this.router.navigate(["semester"]);
      }else {
        // GGWP
      }
    })
  }
}
