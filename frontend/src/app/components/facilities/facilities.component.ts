import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";
import {RequestsService} from "../../services/requests.service";

@Component({
  selector: 'app-facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.scss']
})
export class FacilitiesComponent implements OnInit {
  closeResult: string;
  buildingName: string;
  message: string;
  showAlert: boolean;
  allBuilding: any;
  selectedBuilding: any;
  roomName: string;
  allRooms: any;

  // lets assume there are builiding for now


  constructor(private modalService: NgbModal, private router: Router, private requestsService: RequestsService) { }

  ngOnInit() {
    this.loadBuilding();
    this.loadRoom();
  }

  loadRoom(){
    this.requestsService.getRoom().subscribe((result) => {
      this.allRooms = result;
    });
  }

  loadBuilding() {
    this.requestsService.getBuilding().subscribe((result) => {
      this.allBuilding = result;
    });
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  addBuilding(){
    let building = {
      buildingName: this.buildingName,
    };
    this.requestsService.createBuilding(building).subscribe((res) => {
      if (res == "200") {
        this.message = "Building is added successfully";
        this.showAlert = true;
        this.modalService.dismissAll("success");
        this.loadBuilding();
        this.router.navigate(["facilities"]);
      } else {
        // GGWP
      }
    });
  }
  deleteRoom(roomID){
    let keys = {
      roomID: roomID,
    };
    this.requestsService.deleteRoom(keys).subscribe( (res) =>{
      if (res == "200") {
        this.loadRoom();
        this.router.navigate(["facilities"]);
      }else {
        console.log(res)
        // GGWP
      }
    })
  }

  selectBuilding(buildingID){
    this.selectedBuilding = buildingID;
  }

  addRoom(){
    let room = {
      buildingID: this.selectedBuilding,
      roomName: this.roomName,
    }
    this.requestsService.createRoom(room).subscribe((res) => {
      if (res == "200") {
        this.message = "Room is added successfully";
        this.showAlert = true;
        this.modalService.dismissAll("success");
        this.loadRoom();
        this.router.navigate(["facilities"]);
      } else {
        // GGWP
      }
    });
  }

}
